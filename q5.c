#include<stdio.h>
int main(){
	int A=10,B=15;

	printf("Bitwise AND Operator=%d\n",A&B);
	printf("Bitwise XOR Operator=%d\n",A|B);
	printf("Bitwise Complement Operator=%d\n",~A);
	printf("Bitwise Left shift Operator=%d\n",A<<3);
	printf("Bitwise Right shift Operator=%d\n",A>>3);

	return 0;
}
